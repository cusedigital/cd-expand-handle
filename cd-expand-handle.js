import { LitElement, html, css } from 'lit-element';
import '@polymer/iron-icon';
import './cd-expand-handle-icons.js'

/**
 * `cd-expand-handle`
 * A web component representing a handle to expand and contract content.
 *
 * @customElement
 * @demo demo/index.html
 */

class CdExpandHandle extends LitElement {
  static get properties() {
    return {
      isOpen: {
        type: Boolean,
        attribute: "is-open",
        reflect: true,
      },
      disabled: {
        type: Boolean,
        reflect: true,
      },
      icon: {
        type: String,
        reflect: true,
      },
      //init in constructor like `this.myProp = ...
    };
  }

  /** LIFECYCLE **/
  /**
   * https://developer.mozilla.org/en-US/docs/Web/Web_Components/Using_custom_elements#Using_the_lifecycle_callbacks
   */
  constructor() {
    super();

    this.isOpen = false;
    this.disabled = false;
    this.icon = 'cd-expand-handle:triangle-right';
  }

  connectedCallback() {
    super.connectedCallback();
  }

  firstUpdated() {
    super.firstUpdated();

    // This is a good place to bind event handlers to elements.
    // Hint: Use litaddevent snippet

  }
  /** /LIFECYCLE **/

  /** OBSERVE PROPERTY CHANGES **/
  // eg. _propertyChanged
  /** /OBSERVE PROPERTY CHANGE **/

  /**
   * Styles common to all instances of this component.
   * @return {css} css template literal
   */
  static get styles() {
    return [
      css`
      :host {
        display: block;
      }
      `
    ];
  }

  /** TEMPLATE PARTS */
  // Hint: Use litelhtml snippet
  /** /TEMPLATE PARTS */

  /**
   * The main component template.
   * @return {html} html template literal
   */
  render() {
    return html`
    <style>
      #handle {
          width: 20px;
          height: 20px;
          transform: rotate(var(--cd-expand-handle-open-rotation, 90deg));
          transition: transform var(--cd-expand-handle-duration, .5s) cubic-bezier(.4, 0, .2, 1);
      }

      #container {
        cursor: var(--cd-expand-handle-cursor, pointer);
      }

      .disabled #handle {
          color: var(--cd-expand-handle-disabled-color, #999);
      }

      .closed #handle {
          transform: rotate(var(--cd-expand-handle-closed-rotation, 0deg));
      }
    </style>

    <div id="container" @click="${this._toggle}" class="${this._getStateClass()} ${this._getDisabledClass()}">
      <iron-icon
        id="handle"
        icon="${this.icon}"
      ></iron-icon>
      <slot></slot>
    </div>
    `;
  }

  // Event Handlers
  // eg `onButtonClick()`
  // /Event Handlers

  // METHODS
  // Private methods:

  _getStateClass() {
    return this.isOpen?'':'closed';
  }

  _getDisabledClass() {
      return this.disabled?'disabled':'';
  }

  _toggle() {
    if (!this.disabled) {
      this.isOpen = !this.isOpen;

      if (this.isOpen) {
        let event = new CustomEvent('open', {
          bubbles: true
        });
        this.dispatchEvent(event);
      } else {
        let event = new CustomEvent('close', {
          bubbles: true
        });
        this.dispatchEvent(event);
      }
    }
  }
  // Public methods (component API):

  // /METHODS
}

customElements.define('cd-expand-handle', CdExpandHandle);