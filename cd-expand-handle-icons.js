/**
@license
Copyright (c) 2014 The Polymer Project Authors. All rights reserved.
This code may only be used under the BSD style license found at
http://polymer.github.io/LICENSE.txt The complete set of authors may be found at
http://polymer.github.io/AUTHORS.txt The complete set of contributors may be
found at http://polymer.github.io/CONTRIBUTORS.txt Code distributed by Google as
part of the polymer project is also subject to an additional IP rights grant
found at http://polymer.github.io/PATENTS.txt
*/
import '@polymer/iron-icon/iron-icon.js';
import '@polymer/iron-iconset-svg/iron-iconset-svg.js';

import {html} from '@polymer/polymer/lib/utils/html-tag.js';

/**

`iron-icons` is a utility import that includes the definition for the
`iron-icon` element, `iron-iconset-svg` element, as well as an import for the
default icon set.

The `iron-icons` directory also includes imports for additional icon sets that
can be loaded into your project.

Example loading icon set:

    <script type="module">
      import '@polymer/iron-icons/maps-icons.js';
    </script>

To use an icon from one of these sets, first prefix your `iron-icon` with the
icon set name, followed by a colon, ":", and then the icon id.

Example using the directions-bus icon from the maps icon set:

    <iron-icon icon="maps:directions-bus"></iron-icon>

See [iron-icon](https://www.webcomponents.org/element/@polymer/iron-icon) for
more information about working with icons.

See [iron-iconset](https://www.webcomponents.org/element/@polymer/iron-iconset)
and
[iron-iconset-svg](https://www.webcomponents.org/element/@polymer/iron-iconset-svg)
for more information about how to create a custom iconset.

@group Iron Elements
@pseudoElement iron-icons
@demo demo/index.html
*/

const template = html`<iron-iconset-svg name="cd-expand-handle" size="24">
<svg><defs>
<g id="triangle-right" transform = "rotate(-90 12 12)"><path d="M7 10l5 5 5-5z"></path></g>
</defs></svg>
</iron-iconset-svg>`;

document.head.appendChild(template.content);
